<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit04362b1038c65257c35dee7071d96f73
{
    public static $prefixLengthsPsr4 = array (
        'E' => 
        array (
            'Eluceo\\iCal\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Eluceo\\iCal\\' => 
        array (
            0 => __DIR__ . '/..' . '/eluceo/ical/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit04362b1038c65257c35dee7071d96f73::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit04362b1038c65257c35dee7071d96f73::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}

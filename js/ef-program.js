( function($){
	var efProgrammGrid;
	var thisProgram;
	var pdf = window.efProgramOptions && efProgramOptions.pdf || false;
	var quicksearch = window.efProgramOptions && efProgramOptions.quicksearch || false;
	var switcher = window.efProgramOptions && efProgramOptions.parallel_items || false;

	$(document).ready(function(){
		// add class to body
		$('body').addClass('ef-program-active');

		thisProgram = $('.ef-programm-main-wrapper');

		// event handler
		UIkit.on('domready.uk.dom', efProgrammGridInit);
		$('.ef-dynamic-grid-filter [data-uk-filter] > a').on( 'click', efOnClickDynamicFilter );
		thisProgram.find('.uk-button-group > button').on('click', efOnClickViewButton);

		if ( pdf ){
			UIkit.on('domready.uk.dom', efPrintfriendlyInit);

			$('.printfriendly > a').on('click', function(){
				var t = $(this);
				$(window).on('message', t, efScrollBackAfterPFPreviewClosed);
			});

			if (switcher){
				$('.ef-program-slot-parallel [data-uk-tab]').on( 'show.uk.switcher', efOnSwitchParallelBlocksTab );
			}
		}

		if ( quicksearch ){
			UIkit.on('domready.uk.dom', efProgrammSearchInit);
		}

		if ( switcher ){
			$('.ef-dynamic-grid-filter [data-ef-tab-filter] > a').on( 'click', efOnClickTabFilter );
		}
	});

	function efProgrammGridInit(){
		 efProgrammGrid = UIkit.grid($('.ef-programm > .uk-grid'));
	}

	function efOnClickViewButton(){
		if ( $(this).hasClass('grid') ){
			thisProgram
			.removeClass('ef-programm-list-view').addClass('ef-programm-grid-view')
			.find('.ef-programm > .uk-grid').removeClass('uk-grid-width-1-1').addClass('uk-grid-width-medium-1-2 uk-grid-width-large-1-3').trigger('display.uk.check');
		} else if ( $(this).hasClass('list') ){
			thisProgram
			.removeClass('ef-programm-grid-view').addClass('ef-programm-list-view')
			.find('.ef-programm > .uk-grid').removeClass('uk-grid-width-medium-1-2 uk-grid-width-large-1-3').addClass('uk-grid-width-1-1').trigger('display.uk.check');
		}
		$(this).addClass('uk-active').siblings().removeClass('uk-active');
	}

	function efOnClickDynamicFilter(){

		/*----------  scroll to top of dynamic grid after filtering  ----------*/
		var efDynamicGridOffset = thisProgram.offset().top - 15;
		$('html, body').scrollTop(efDynamicGridOffset);	

		// prepare print-friendly output
		efPreparePrintFriendlyOutput.apply(this);
	}

	function efOnClickTabFilter(e){

		e.preventDefault();

		var filterID = $(this).parent().data('ef-tab-filter');
		var block = $('.ef-programm-parallel-item[data-ef-tab-filter="'+filterID+'"]');
		var blockIndex = block.index();
		var parentSlot = block.closest('.ef-program-slot-parallel');
		var parentSlotGridFilterID = parentSlot.attr('data-uk-filter');
		var switcher = UIkit.switcher( parentSlot.find('[data-uk-tab]') );
		var animation = efProgrammGrid.options.animation;
		var duration = efProgrammGrid.options.duration;

		// add tab filter id to grid filter ids
		if ( parentSlotGridFilterID.indexOf(filterID) == -1 ){
			parentSlot.attr('data-uk-filter', parentSlotGridFilterID + ',' + filterID);
		}

		efProgrammGrid.element.one('afterupdate.uk.grid', function(){
			switcher.show(blockIndex);
			efProgrammGrid.options.animation = animation;
			efProgrammGrid.options.duration = duration;

		});
		efProgrammGrid.options.animation = false;
		efProgrammGrid.options.duration = 0;
		efProgrammGrid.filter(filterID);
	}

	function efOnSwitchParallelBlocksTab(){
		if (!efProgrammGrid){
			return;
		}
		var $this = $(this);
		var currentFilter = efProgrammGrid.currentfilter;

		// set printfriendly classes to selected tab (only if exactly one progam slot is visible)
		if ( currentFilter && efProgrammGrid.element.find('[data-uk-filter*='+currentFilter+']').length === 1 ){
		
			printFriendlyResetSelection();

			setTimeout( function(){
				$this.closest('.ef-programm-parallel').find('.ef-parallel-blocks').addClass('print-only').find('> *:not(.uk-active)').addClass('print-no');
			}, 100);
		}
	}

	function efPrintfriendlyInit(){
		var activeFilter = $('.ef-dynamic-grid-filter > li.uk-active > a');
		efPreparePrintFriendlyOutput.apply(activeFilter);
	}

	function efPreparePrintFriendlyOutput(){
		var efDynamicGridFilter = $(this).closest('.ef-dynamic-grid-filter');
		var efDynamicGridFilterID = efDynamicGridFilter.attr('id'); 
		var efDynamicGrid = $( '[data-uk-grid*=' + efDynamicGridFilterID );
		var thisFilterID = $(this).parent().data('uk-filter');
		
		/*----------  Populate filter title dynamically (for printfriendly output)  ----------*/
		
		var title = $(this).parent().attr('data-uk-filter') === '' ? '' : $(this).text();

		/*----------  Add print-only class to selected grid items  ----------*/
		printFriendlyResetSelection();

		if ( thisFilterID == '' ){ // click on 'all'
			efDynamicGrid.find('> div').addClass('print-only');
		} else {
			efDynamicGrid.find('> div[data-uk-filter*='+ thisFilterID +']').addClass('print-only');
		}
		$('.ef-programm-pf-filter-title').text(title).addClass('print-only');
	}

	function printFriendlyResetSelection(){
		$('.print-only').removeClass('print-only');
		$('.print-no').removeClass('print-no');
	}

	function efScrollBackAfterPFPreviewClosed(e){

		if ( e.originalEvent.data.hasOwnProperty('type') && e.originalEvent.data.type === "PfClosePreview" && e.data instanceof jQuery ){
			var offset = e.data.offset().top - 50;
			$('html, body').scrollTop(offset);
			$(window).off('message', 'efScrollBackAfterPFPreviewClosed');
		}
	}

	function efProgrammSearchInit(){
		// initialize
		var $efProgrammSearch = $('[id^=ef-programm-search]');
		var $efProgrammGrid = efProgrammGrid.element;
		var $efProgrammTexts = $efProgrammGrid.find('> div > div');
		var firstFilter = [];
		var filter = [];
		var result = [];
		$efProgrammTexts.each(function() {
			// store original filter ids
		    firstFilter.push($(this).closest('[data-uk-filter]').attr('data-uk-filter'));
		    // store text
		    filter.push($(this).text());
		});

		// add original filter ids to each text
		for (var i = 0; i < filter.length; i++) {
		    filter[i] = filter[i] + ' ' + (firstFilter[i]);
		}

		$efProgrammSearch.keydown(function(e) {

			// prevent form submission (and page reload) on enter
			if (e.key == 'Enter'){
				e.preventDefault();
			}
			// clear search field with escape
			if (e.key == 'Escape'){
				$efProgrammSearch.val('');
			}
		});

		$efProgrammSearch.keyup(function() {

		    var textinput = $(this).val();
		    var re = new RegExp(textinput, 'i');

		    if (textinput != '') {
		    	// build result array by matching each text item against the regexed search input
		        filter.forEach(function(item, i, filter) {
		        	// store matches as arrays containing matching strings and mismatches as null
		            result[i] = item.match(re);
		        });
		        if (pdf){
			        // reset print-only items
			        printFriendlyResetSelection();
			    }

		        $efProgrammTexts.each(function(index, el) {
		        	var $thisItem = $(this).closest('[data-uk-filter]');
		        	if (result[index] != null) {
		        		// add text input as filter criteria and select items for printing
		        		$thisItem.attr('data-uk-filter', textinput + ',' + firstFilter[index]);
		        		if (pdf){
		        			$thisItem.addClass('print-only');
		        		}
		        	} else {
		        		// add back original filter ids 
		        		$thisItem.attr('data-uk-filter', firstFilter[index]);
		        	}
		        });
		    } else {
		    	 $efProgrammTexts.each(function(index, el) {
	    	 		// add back original filter ids 
	    			$(this).closest('[data-uk-filter]').attr('data-uk-filter', firstFilter[index]);
	    			if (pdf){
		    			// reset print-only items
		    			printFriendlyResetSelection();
		    		}
		    	});
		    }

		    // filter grid items
		    efProgrammGrid.filter(textinput);

		});

		// clear search input when a filter button is clicked
		$('.ef-dynamic-grid-filter a').on('click', function(){
			$efProgrammSearch.val('');
		});
	}

})(jQuery);
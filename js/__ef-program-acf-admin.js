( function($){

	var efProgram;
	var efProgramTime;
	var efProgramAutoTimeSwitch;
	var efProgramAutoTime;


	// action triggered on page load (when acf is ready)
	acf.addAction( 'ready', function(){

		efProgramAutoTimeSwitch = acf.getField('field_5cb87f6e85c26');

		efProgramAutoTimeSwitch.$el.on('change', function(){
			efProgramAutoTime = efProgramAutoTimeSwitch.val();
			if ( efProgramAutoTime ){
				updateTimeFields();
				efProgramTime.forEach( function(field){
					field.set('time', field.val() );
				});
			}
		});

		// store ef program flexible content field object
		efProgram = acf.getField('field_5c0fda8cfbfb6');

		if ( ! efProgram.$el.length ){
			return;
		}

		// store current time field objects of efProgram in efProgramTime
		updateTimeFields();

		setTimeout( function(){
			// add listener to change event of time input fields
			efProgramTime.forEach( function(field){
				addChangeListener(field);
				addSortStopListener(field);
				field.set('time', field.val() );
			});
		}, 500);

	});

	// action triggered when a new time field is added
	acf.addAction( 'append_field/key=field_5c0fdb00fbfb7', function(field){

		updateTimeFields();
		addChangeListener(field);
		addSortStopListener(field);
	});

	// action triggered when a time field is removed
	acf.addAction( 'remove_field/key=field_5c0fdb00fbfb7', function(field){

		var index = efProgramTime.indexOf(field);
		var time = field.$inputText().val();
		var next = efProgramTime[index + 1];

		// remove field from time field array
		efProgramTime.splice( index, 1 );

		if (next){
			next.$inputText().datepicker( 'setTime', time );
			next.$inputText().trigger('efProgramTimeChange');
		}
	});

	// filter the time picker arguments (to add a custom event trigger when the time picker is closed)
	acf.add_filter('time_picker_args', function( args, field ){

	    // add custom 'Close = Select' functionality
	    args.onClose = function( value, dp_instance, t_instance ){

	    	// add custom event trigger
	    	t_instance.$input.trigger('efProgramTimeChange');

	    	// vars
	    	var $close = dp_instance.dpDiv.find('.ui-datepicker-close');
	    	
	    	// if clicking close button
	    	if( !value && $close.is(':hover') ) {
	    		t_instance._updateDateTime();
	    	}				
	    };

	    // return
	    return args;

	});

	// filter the speaker ajax request data to enable dynamic taxonomy filtering
	acf.addFilter('select2_ajax_data', function(ajaxData, data, $el, field, _this){

		if ( field.data.key != 'field_5c0fdb9ffbfb9'){
			return ajaxData;
		}

		var catField = acf.getFields(
			{
				key: 'field_5ce846f55994d',
				type: 'taxonomy',
				sibling: $el.closest('.acf-field')
			}
		);
		var cat = catField[0].val();

		if (cat !== ''){
			ajaxData.s += ':cat:'+cat;
		}

		return ajaxData;
	});

	// add custom efProgramTimeChange event listener to time fields
	function addChangeListener(field){
		// add listener to input fields [type=text] and pass the field object as data
		efProgram.$el.on('efProgramTimeChange', '#' + field.$inputText().attr('id'), field, onTimeChange );
	}

	function addSortStopListener(field){

		field.on('sortstopField', function(){

			if ( ! efProgramAutoTime ){
				return;
			}

			var indexOld = efProgramTime.indexOf(field);
			var nextOld = efProgramTime[indexOld + 1];
			var thisOldTime;
			var nextOldTime;
			var thisLength;
			var indexNew;
			var nextNew;
			var thisTimeNew;

			thisOldTime = getDateFromTimeString( field.$inputText().val() );

			if ( nextOld ){
				nextOldTime = getDateFromTimeString( nextOld.$inputText().val() );
			}

			if ( thisOldTime && nextOldTime ){
				thisLength = nextOldTime - thisOldTime;
			}

			updateTimeFields();

			indexNew = efProgramTime.indexOf(field);
			nextNew = efProgramTime[indexNew + 1];

			// exit if position is unchanged
			if ( indexOld === indexNew ){
				return;

			} else if ( indexOld > indexNew ){

				thisTimeNew = nextNew.$inputText().val();
				field.$inputText().datepicker('setTime', thisTimeNew);
				field.set('time', thisTimeNew);
				efProgram.renderLayout( field.$el.closest('.layout') );

				if ( thisLength ){

					for (var i = indexNew + 1; i < efProgramTime.indexOf(nextOld); i++) {
						
						setTimeForNext(efProgramTime[i], thisLength);
					}
				}

			} else if ( indexOld < indexNew ){

				if ( thisLength ){

					for (var i = indexOld; i < indexNew; i++) {
						
						setTimeForNext(efProgramTime[i], -thisLength);
					}

					thisTimeNew = getDateFromTimeString( nextNew.$inputText().val() );

					if ( thisTimeNew ){
						thisTimeNew.setTime( thisTimeNew.getTime() - thisLength );
						field.$inputText().datepicker('setTime', thisTimeNew);
						field.set('time', pad(thisTimeNew.getHours()) + ':' + pad(thisTimeNew.getMinutes()) );
						efProgram.renderLayout( field.$el.closest('.layout') );
					}

				}

			}
			
		});
	}

	function onTimeChange(e){

		if ( ! efProgramAutoTime ){
			return;
		}

		var field = e.data; // field object is passed by addChangeListener()
		var index = efProgramTime.indexOf(field);
		var next;
		var thisTimeNew;
		var thisTimeOld;
		var timeDiff;
		var nextTime;
		var nextTimeNew;

		if ( index > -1 ){
			
			// get current time value
			thisTimeNew = field.$inputText().val();

			// get stored time value
			thisTimeOld = field.get('time')

			// create Date object
			thisTimeNew = getDateFromTimeString(thisTimeNew);

			// exit if thisTimeNew is not set or has a wrong format (in this case, getDateFromTimeString returns undefined)
			if ( ! thisTimeNew ){
				return;
			}

			thisTimeOld = getDateFromTimeString(thisTimeOld);

			// if there's no time value stored, store the new value and exit
			if ( ! thisTimeOld ){

				field.set('time', pad(thisTimeNew.getHours()) + ':' + pad(thisTimeNew.getMinutes()) );
				return;
			}

		}

		// get time difference of old and new time
		timeDiff = thisTimeNew.getTime() - thisTimeOld.getTime();

		// exit if time hasn't changed
		if ( timeDiff === 0 ){	
			return;
		}

		// store new time value
		field.set( 'time', pad(thisTimeNew.getHours()) + ':' + pad(thisTimeNew.getMinutes()) );

		// refresh acf flexible content fields (triggers update of collapsed layout label)
		efProgram.renderLayout( field.$el.closest('.layout') );

		// exit if last item was modified
		if ( index >= efProgramTime.length - 1){
			return;
		}

		// loop through following time fields and add the value of timeDiff
		for (var i = index + 1; i < efProgramTime.length; i++) {
			
			setTimeForNext(efProgramTime[i], timeDiff);
			
		}
	}

	function updateTimeFields(){
		efProgramTime = acf.getFields({
			type: 'time_picker',
			name: 'time',
			parent: efProgram.$el
		});
	}

	function setTimeForNext(next, timeDiff){

		var nextTime = getDateFromTimeString( next.get('time') );
		if ( ! nextTime ){
			return;
		}
		nextTime.setTime( nextTime.getTime() + timeDiff );
		next.$inputText().datepicker( 'setTime', nextTime );
		efProgram.renderLayout( next.$el.closest('.layout') );
		next.set( 'time', pad(nextTime.getHours()) + ':' + pad(nextTime.getMinutes()) );
	}

	function pad(n){
		return n < 10 ? '0' + n : n;
	}

	function getDateFromTimeString(time){
		if ( typeof time !== 'string' || time.indexOf(':') === -1 ) {
				return false;
		}
		time = time.split(':');
		time = new Date( 0,0,0,time[0], time[1] );

		return time;
	}

})(jQuery)
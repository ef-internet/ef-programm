( function($){

	var efProgram;
	var efProgramTime;
	var efProgramTimeEnd;
	var efProgramAutoTimeSwitch;
	var efProgramAutoTime;
	var indexHasChanged = false;

	// register sorting of time slots (only when an element is really moved to another position)
	$(document).on('sortchange', registerIndexChange);


	// action triggered on page load (when acf is ready)
	acf.addAction( 'ready', function(){

		efProgramAutoTimeSwitch = acf.getField('field_5cb87f6e85c26');
		efProgramAutoTime = efProgramAutoTimeSwitch.val();

		efProgramAutoTimeSwitch.$el.on('change', function(){
			efProgramAutoTime = efProgramAutoTimeSwitch.val();
			if ( efProgramAutoTime ){
				updateTimeFields();
				efProgramTime.forEach( function(field){
					field.set('time', getDateFromTimeString(field.val()) );
				});
				efProgramTimeEnd.forEach( function(field){
					field.set('time', getDateFromTimeString(field.val()) );
				});
			}
		});

		// store ef program flexible content field object
		efProgram = acf.getField('field_5c0fda8cfbfb6');

		if ( ! efProgram.$el.length ){
			return;
		}

		// store current time field objects of efProgram in efProgramTime and efProgramTimeEnd
		updateTimeFields();

		setTimeout( function(){
			// add listener to change event of time input fields
			efProgramTime.forEach( function(field){
				addChangeListener(field);
				addSortStopListener(field);
				addChangeListenerForSlotHeader(field);
				field.set('time', getDateFromTimeString(field.val()) );
				updateSlotHeader(field);
			});
			efProgramTimeEnd.forEach(function(field){
				addTimeEndChangeListener(field);
				field.set('time', getDateFromTimeString(field.val()) );
			});
		}, 500);

	});

	// action triggered when a new time field is added
	acf.addAction( 'append_field/key=field_5c0fdb00fbfb7', function(field){

		updateTimeFields();
		addChangeListener(field);
		addChangeListenerForSlotHeader(field);
		addSortStopListener(field);
		setInitialStartTime(field);
	});

	// action triggered when a new time end field is added
	acf.addAction( 'append_field/key=field_5dd7eb9b19351', function(field){
		addTimeEndChangeListener(field);
	});
	
	// action triggered when a type field is loaded
	acf.addAction( 'load_field/key=field_5c123413990a4', function(field){
		addChangeListenerForSlotHeader(field);
	});

	// action triggered when a new type field is added
	acf.addAction( 'append_field/key=field_5c123413990a4', function(field){

		addChangeListenerForSlotHeader(field);
	});
	// action triggered when a title field is loaded
	acf.addAction( 'load_field/key=field_5c0fdb7ffbfb8', function(field){
		addChangeListenerForSlotHeader(field);
	});

	// action triggered when a new title field is added
	acf.addAction( 'append_field/key=field_5c0fdb7ffbfb8', function(field){

		addChangeListenerForSlotHeader(field);
	});

	// action triggered when a time field is removed
	acf.addAction( 'remove_field/key=field_5c0fdb00fbfb7', function(field){
		console.log(field);

		var index = efProgramTime.indexOf(field);
		var time = field.get('time');
		var next = efProgramTime[index + 1];

		if ( isItemParallel(field) ){
			console.log('parallel!');
			var parallelBlock = field.$el.closest('[data-name="parallel-item"]');
			var firstParallelIndex = getFirstParallelIndex(field);
			var lastParallelIndex = getLastParallelIndex(field);

			// if the removed field was a parallel item and the last one in its block, just update the time field arrays and break
			if ( lastParallelIndex === efProgramTime.indexOf(field) ){
				console.log('lastparalleItem!');

				// when updateParallelBlockTimeEnd() is called, the DOM is already changed and the selectors no longer work
				// jQuery object (parallel block) passed instead of acf field
				// wait for the dom elements to be removed before checking/updating parallel block time end
				setTimeout(function(){
					updateParallelBlockTimeEnd(parallelBlock);
				}, 500);
			}
			if ( firstParallelIndex === efProgramTime.indexOf(field) ){
				// wait for the dom elements to be removed before checking/updating parallel block time start
				setTimeout(function(){
					updateParallelBlockTimeStart(parallelBlock);
				}, 500);
			}
			// remove start and end fields from time field arrays
			efProgramTime.splice( index, 1 );
			efProgramTimeEnd.splice( index, 1 );
			return;
		} else {
			if ( next && isItemParallel(next) ){
				console.log('nextisparallel');
				console.log(next);
				var parent = field.$el.closest('.layout');
				next = getNextProgramSlotField(field, 'time');
			}
		}

		// set time of next item (for normal program slots and non-last-child-parallel items )
		if (next){
			console.log(next);
			next.$inputText().datepicker( 'setTime', time );
			next.$inputText().trigger('efProgramTimeChange');
			console.log(time);
		}
	});

	// filter the time picker arguments (to add a custom event trigger when the time picker is closed)
	acf.add_filter('time_picker_args', function( args, field ){

	    // add custom 'Close = Select' functionality
	    args.onClose = function( value, dp_instance, t_instance ){

	    	// add custom event trigger
	    	t_instance.$input.trigger('efProgramTimeChange');

	    	// vars
	    	var $close = dp_instance.dpDiv.find('.ui-datepicker-close');
	    	
	    	// if clicking close button
	    	if( !value && $close.is(':hover') ) {
	    		t_instance._updateDateTime();
	    	}				
	    };

	    // return
	    return args;

	});

	// filter the speaker ajax request data to enable dynamic taxonomy filtering
	acf.addFilter('select2_ajax_data', function(ajaxData, data, $el, field, _this){

		if ( field.data.key != 'field_5c0fdb9ffbfb9'){
			return ajaxData;
		}

		var catField = acf.getFields(
			{
				key: 'field_5ce846f55994d',
				type: 'taxonomy',
				sibling: $el.closest('.acf-field')
			}
		);
		var cat = catField[0].val();

		if (cat !== ''){
			ajaxData.s += ':cat:'+cat;
		}

		return ajaxData;
	});

	// add custom efProgramTimeChange event listener to time fields
	function addChangeListener(field){
		// add listener to input fields [type=text] and pass the field object as data
		efProgram.$el.on('efProgramTimeChange', '#' + field.$inputText().attr('id'), field, onTimeChange );
	}

	function addTimeEndChangeListener(field){
		efProgram.$el.on('efProgramTimeChange', '#' + field.$inputText().attr('id'), field, onTimeEndChange );
	}

	function addChangeListenerForSlotHeader(field){

		field.on('change', function(){
			updateSlotHeader(field);
		});
	}

/*	function addSortStartListener(field){
		field.on('sortstartField', function(e){

			console.log(e);
			if ( ! field.val() ){
				console.log(field.$el.closest('.values'));
				field.$el.closest('.values').sortable('disable');
				field.showNotice({
					text: 'Es ist keine Endzeit angegeben.',
					type: 'error'
				});
			}

		});
	}*/

	// set change indicator (event fires only on change)
	function registerIndexChange(e, ui){
		if (ui.item.data('layout') && (ui.item.data('layout').indexOf('ef-program-slot') > -1 || ui.item.data('layout').indexOf('parallel-sub-item') > -1)){
			indexHasChanged = true;
		}
	}

	function addSortStopListener(field){

		field.on('sortstopField', function(){
			console.log('sorting');
			console.log(field);
			// break early if nothing changed or autoupdating is deactivated
			if ( ! efProgramAutoTime || ! indexHasChanged ){
				return;
			}

			var thisOldTime; // start time value of moved element (before movement)
			var indexOld = efProgramTime.indexOf(field); // index of moved element (before movement)
			var nextOld = efProgramTime[indexOld + 1]; // field object of following element (before movement)
			var thisOldEnd; // array containing the field object of the moved time end field
			var thisOldEndTime; // time string either of next start time element or current time end element (before movement)
			var thisLength; // duration of current element as integer representing the difference between thisOldEndTime and thisOldTime
			var indexNew; // index of moved element (after movement)
			var nextNew; // field object of following element (after movement)
			var thisTimeNew; // date object either of next start time element or previous time end element (after movement)
			var thisTimeEndNew; // date object
			var isParallelBlock;
			var isParallelItem = isItemParallel(field); // boolean indicating if current item is inside of a parallel block
			var lastParallelIndex;
			if ( isParallelItem ){
				lastParallelIndex = getLastParallelIndex(field);
			}
			console.log('indexOld');
			console.log(indexOld);
			thisOldTime = getDateFromTimeString( field.val() );
			console.log(thisOldTime);
			thisOldEnd = acf.getFields({
				key: 'field_5dd7eb9b19351',
				sibling: field.$el,
				limit: 1
			});

			thisOldEndTime = getDateFromTimeString(thisOldEnd[0].val());
			if ( thisOldTime && thisOldEndTime ){
				thisLength = thisOldEndTime - thisOldTime;
				console.log('thisLength');
				console.log(thisLength / (1000 * 60));
			}

			// if moved element is not a parallel item, but next time field is part of a parallel block
			if (!isParallelItem && nextOld && isItemParallel(nextOld)){
				for (var i = efProgramTime.indexOf(nextOld); i < efProgramTime.length; i++){
					// find next main program slot (before movement)
					if (efProgramTime[i].$el.closest('.layout').data('layout').indexOf('ef-program-slot') > -1 ){
						nextOld = efProgramTime[i];
						break;
					} else {
						// if no next main element exists, set nextOld to null
						nextOld = null;
					}
				}
			}

			updateTimeFields();

			indexNew = efProgramTime.indexOf(field);
			nextNew = efProgramTime[indexNew + 1];
			if (!isParallelItem && nextNew && isItemParallel(nextNew)){
				isParallelBlock = true;
				var parent = field.$el.closest('.layout');
				nextNew = getNextProgramSlotField(field, 'time');
				console.log('nextNew is parallel');
				console.log(nextNew);
			}

			// exit if index is unchanged (change event has fired but sorting didn't affect other time fields)
			if ( indexOld === indexNew ){
				console.log('unchanged');
				return;
			}
/*			if ( nextNew ){
				thisTimeNew = getDateFromTimeString(nextNew.val());
			} else if (efProgramTimeEnd[indexNew - 1]){
				thisTimeNew = getDateFromTimeString(efProgramTimeEnd[indexNew - 1].val());
			} */

			if ( indexOld > indexNew ){
				console.log('indexOld > indexNew');
				if (nextNew){
					thisTimeNew = getDateFromTimeString(nextNew.val());
					console.log('nextnew:');
					console.log(nextNew.val());
					console.log(thisTimeNew);
				}
				field.$inputText().datepicker('setTime', thisTimeNew);
				console.log('field.get("time") vor sorting old > new');
				console.log(field.get('time'));
				console.log(field);
				field.set('time', thisTimeNew);
				//field.set('time', new Date( 0,0,0,11,00));
				console.log('field.get("time") nach sorting old > new');
				console.log(field.get('time'));
				console.log(field);
				//efProgram.renderLayout( field.$el.closest('.layout') );

				if ( thisTimeNew ){
					thisTimeEndNew = new Date();
					thisTimeEndNew.setTime( thisTimeNew.getTime() + thisLength);
				}

				thisOldEnd[0].$inputText().datepicker('setTime', thisTimeEndNew);
				thisOldEnd[0].set('time', thisTimeEndNew);

				if (isParallelBlock){
					var timeDiff = thisOldTime.getTime() - thisTimeNew.getTime();
					for (var i = indexNew + 1; i < efProgramTime.indexOf(nextNew); i++){
						setTime(efProgramTime[i], -timeDiff);
						setTime(efProgramTimeEnd[i], -timeDiff);
					}
				}


				console.log('thisLength');
				console.log(thisLength / (1000 * 60));
				if ( thisLength > 0 ){
					var limit;
					if (! isParallelItem ){
						// if the moved element was the last item (nextOld doesn't exist), set limit to length of efProgramTime array
						// else set limit to index position of next item (before movement)
						limit = nextOld ? efProgramTime.indexOf(nextOld) : efProgramTime.length;
						console.log('limit:');
						console.log(limit);
					} else {
						// if the moved element was the last one in its parallel block, set limit of time change loop to its old index plus 1
						// else set the limit to the index of the next element (before movement)
						limit = indexOld === lastParallelIndex ? indexOld + 1 : efProgramTime.indexOf(nextOld);
					}

					for (var i = efProgramTime.indexOf(nextNew); i < limit; i++) {
						setTime(efProgramTime[i], thisLength);
						setTime(efProgramTimeEnd[i], thisLength);
						console.log('set time for next: i');
						console.log(i);
						console.log(efProgramTime[i]);
					}
				}

			} else if ( indexOld < indexNew ){

				if ( thisLength ){
					if ( !isParallelItem && isItemParallel(efProgramTimeEnd[indexNew - 1]) ){
						var prevMain = getPrevProgramSlotField(field, 'time_end');
						if (prevMain.$el.length){
							thisTimeNew = getDateFromTimeString(prevMain.val());
						}
					} else {
						thisTimeNew = getDateFromTimeString(efProgramTimeEnd[indexNew - 1].val());
					}
					// update time of following items until new index position is reached (later items aren't changed)
					console.log('indexOld:');
					console.log(indexOld);
					console.log('indexNew');
					console.log(indexNew);
					for (var i = indexOld; i < indexNew; i++) {
						
						setTime(efProgramTime[i], -thisLength);
						setTime(efProgramTimeEnd[i], -thisLength);
					}

					if ( thisTimeNew ){
						thisTimeEndNew = new Date();
						thisTimeEndNew.setTime( thisTimeNew.getTime() );
						thisTimeNew.setTime( thisTimeNew.getTime() - thisLength );
						field.$inputText().datepicker('setTime', thisTimeNew);
						field.set('time', thisTimeNew);
						thisOldEnd[0].$inputText().datepicker('setTime', thisTimeEndNew);
						thisOldEnd[0].set('time', thisTimeEndNew);

						if (isParallelBlock){
							var timeDiff = thisTimeNew.getTime() - thisOldTime.getTime();
							var parallelItems = acf.findFields({
								name: 'time',
								parent: field.$el.closest('.layout').find('[data-name="parallel-item"]')
							});

							for (var i = indexNew + 1; i <= indexNew + parallelItems.length; i++){
								setTime(efProgramTime[i], timeDiff);
								setTime(efProgramTimeEnd[i], timeDiff);
							}
						}
					}

				}

			}
			// reset index change indicator
			indexHasChanged = false;
			
		});
	}

	function updateSlotHeader(field){

		var parent = field.$el.closest('.layout');
		var title = acf.getFields({
			key: 'field_5c0fdb7ffbfb8',
			parent: parent,
			limit: 1
		});
		var time = acf.getFields({
			key: 'field_5c0fdb00fbfb7',
			parent: parent,
			limit: 1
		});
		var type = acf.getFields({
			key: 'field_5c123413990a4',
			parent: parent,
			limit: 1
		});
		var slotHeader;
		var slotHeaderElements = '';

		slotHeader = parent.find('.acf-fc-layout-handle').eq(0);
		slotHeaderElements += time ? '<span class="ef-program-time-label">'+time[0].$inputText().val()+'</span>' : '';
		slotHeaderElements += type[0].val() ? '<span class="ef-program-type-label">'+type[0].select2.$el.text()+'</span>' : '';
		slotHeaderElements += title[0].val() ? '<span class="ef-program-title-label">'+title[0].val()+'</span>' : '';

		if ( ! slotHeader.find('.ef-program-slot-header').length ){
			slotHeader.find('.acf-fc-layout-order').remove();
			slotHeaderElements += '<span class="ef-program-layout-label">'+slotHeader.text()+'</span>';
		} else {
			slotHeaderElements += slotHeader.find('.ef-program-layout-label').prop('outerHTML');
		}
		slotHeader.html('<span class="ef-program-slot-header">'+slotHeaderElements+'</span>');
	}

	function onTimeChange(e){

		var field = e.data; // field object is passed by addChangeListener()
		var index = efProgramTime.indexOf(field);
		var next;
		var thisTimeNew;
		var thisTimeOld;
		var timeDiff;
		var nextTime;
		var nextTimeNew;
		var isParallelItem = isItemParallel(field);

		if ( index > -1 ){
			
			// get current time value
			thisTimeNew = getDateFromTimeString(field.val());
			console.log('onTimeChange: get current time value');
			console.log(field);
			console.log(field.val());
			// get stored time value
			thisTimeOld = field.get('time');
			console.log('thisTimeOld');
			console.log(thisTimeOld);

			// exit if thisTimeNew is not set or has a wrong format (in this case, getDateFromTimeString returns undefined)
			if ( ! thisTimeNew ){
				return;
			}

			// store new time value
			field.set( 'time', thisTimeNew );

			if (isParallelItem){
				var firstParallelIndex = getFirstParallelIndex(field);
				var lastParallelIndex = getLastParallelIndex(field);
			}

			// if there's no time value stored, exit
			if ( ! thisTimeOld ){
				return;
			}

		}

		// get time difference of old and new time
		timeDiff = thisTimeNew.getTime() - thisTimeOld.getTime();

		// exit if time hasn't changed
		if ( timeDiff === 0 ){	
			return;
		}

		// refresh acf flexible content fields (triggers update of collapsed layout label)
		//efProgram.renderLayout( field.$el.closest('.layout') );

		// exit if last item was modified
/*		if ( index >= efProgramTime.length - 1){
			return;
		}*/

		// exit if autotime option is not enabled
		if ( ! efProgramAutoTime ){
			return;
		}

		if (timeDiff > 0){

/*			var prevMainEnd = getPrevProgramSlotField(field, 'time_end');
			prevMainEnd.$inputText().datepicker('setTime', thisTimeNew);

			preventParallelBlockTimeChange(prevMainEnd);*/

			// loop through time fields and add the value of timeDiff
			for (var i = isParallelItem ? firstParallelIndex : 0; i < (isParallelItem ? lastParallelIndex + 1 : efProgramTime.length); i++) {
				
				// skip initial item start time (already set by timepicker)
				if (i !== index){
					// start time
					setTime(efProgramTime[i], timeDiff);
				}
				if (isParallelItem && i === firstParallelIndex){
					updateParallelBlockTimeStart(efProgramTime[i]);
				}
				// end time
				setTime(efProgramTimeEnd[i], timeDiff);
				
				if (isParallelItem && i === lastParallelIndex){
					updateParallelBlockTimeEnd(efProgramTimeEnd[i]);
				}
			}
		}

		if (timeDiff < 0){
			// loop through time fields and add the (negative) value of timeDiff
			for (var i = isParallelItem ? firstParallelIndex : 0; i < index + 1; i++){
				// start time
				// skip initial item start time (already set by timepicker)
				if (i !== index){
					setTime(efProgramTime[i], timeDiff);
				}
				if (isParallelItem && i === firstParallelIndex){
					updateParallelBlockTimeStart(efProgramTime[i]);
				}
				// end time
				setTime(efProgramTimeEnd[i], timeDiff);
				if (isParallelItem && i === lastParallelIndex){
					updateParallelBlockTimeEnd(efProgramTimeEnd[i]);
				}
			}
		}
	}

	function onTimeEndChange(e){

		console.log('onTimeEndChange');
		// exit if autotime option is not enabled
		if ( ! efProgramAutoTime ){
			return;
		}

		var field = e.data;
		var index =	efProgramTimeEnd.indexOf(field);
		console.log('time end change');
		console.log(field);
		var thisTimeNew = getDateFromTimeString( field.val() );
		var thisTimeOld = field.get('time');
		var next = efProgramTime[index + 1];
		var isParallelItem = isItemParallel(field);
		if (isParallelItem){
			var lastParallelIndex = getLastParallelIndex(field);
		}
		var timeDiff = 0;
		if (!thisTimeOld && next){
			thisTimeOld = getDateFromTimeString(next.val());
		}
		if (thisTimeOld){
			timeDiff = thisTimeNew.getTime() - thisTimeOld.getTime();
		}

		field.set('time', thisTimeNew);

		if ( timeDiff && next ){
			if (!isParallelItem && isItemParallel(next)){
				var parent = field.$el.closest('.layout');
				next = getNextProgramSlotField(field, 'time');
			}

			for (var i = efProgramTime.indexOf(next); i < (isParallelItem ? lastParallelIndex + 1 : efProgramTime.length); i++){

				setTime(efProgramTime[i], timeDiff);
				setTime(efProgramTimeEnd[i], timeDiff);
			}
		}

		if (isParallelItem){
			updateParallelBlockTimeEnd(field);
		}
	}


	function updateTimeFields(){
		efProgramTime = acf.getFields({
			type: 'time_picker',
			name: 'time',
			parent: efProgram.$el
		});
		efProgramTimeEnd = acf.getFields({
			type: 'time_picker',
			name: 'time_end',
			parent: efProgram.$el
		});
	}

	function setTime(field, timeDiff){

		var time = getDateFromTimeString(field.val());
		if (time){
			time.setTime( time.getTime() + timeDiff );
			field.$inputText().datepicker( 'setTime', time );
			field.set('time', time);
		}
	}

	function updateParallelBlockTimeStart(field){
		var parallelBlock = field instanceof jQuery ? field : field.$el.closest('[data-name="parallel-item"]');
		var parallelBlockTimeStart = acf.getField(parallelBlock.closest('[data-layout="ef-program-slot-parallel"]').find('[data-name="time"]').first());
		var timeOld = parallelBlockTimeStart.get('time');
		var firstItems = acf.getFields({
			name: 'time',
			parent: parallelBlock.find('.layout:first-child')
		});
		var earliest = getDateFromTimeString('23:59');
		$.each(firstItems, function(i, item){
			var thisTime = item.get('time');
			console.log('thisEarliestTime:');
			console.log(thisTime);
			if (thisTime.getTime() < earliest.getTime()){
				earliest.setTime(thisTime);
			}
		});
		parallelBlockTimeStart.$inputText().datepicker('setTime', earliest);
		parallelBlockTimeStart.set('time', earliest);
		var timeDiff = earliest.getTime() - timeOld.getTime();

		if (!timeDiff){
			return;
		}

		var prevMain =  acf.getField(parallelBlock.closest('[data-layout="ef-program-slot-parallel"]').prevAll('[data-layout^="ef-program-slot"]').first().find('[data-name="time"]'));
		if (prevMain.$el.length){
			for (var i = efProgramTime.indexOf(prevMain); i >= 0; i--){
				console.log('efProgramTime[i]: '+i);
				console.log(efProgramTime[i]);
				setTime(efProgramTime[i], timeDiff);
				setTime(efProgramTimeEnd[i], timeDiff);
			}
		}
	}

	function updateParallelBlockTimeEnd(field){

		var parallelBlock = field instanceof jQuery ? field : field.$el.closest('[data-name="parallel-item"]');
		console.log(field);
		console.log(parallelBlock);
		var parallelBlockTimeEnd = acf.getField(parallelBlock.closest('[data-layout="ef-program-slot-parallel"]').find('[data-name="time_end"]').first());
		var timeOld = parallelBlockTimeEnd.get('time');
		var lastItems = acf.getFields({
			name: 'time_end',
			parent: parallelBlock.find('.layout:last-child')
		});
		var latest = getDateFromTimeString('00:00');
		$.each(lastItems, function(i, item){
			var thisTime = item.get('time');
			console.log('thisLatestTime:');
			console.log(thisTime);
			if (thisTime.getTime() > latest.getTime()){
				latest.setTime(thisTime);
			}
		});
		parallelBlockTimeEnd.$inputText().datepicker('setTime', latest);
		parallelBlockTimeEnd.set('time', latest);
		var timeDiff = latest.getTime() - timeOld.getTime();

		if (!timeDiff){
			return;
		}
		// update start time of next main program slot
		var nextMain = acf.getField(parallelBlock.closest('[data-layout="ef-program-slot-parallel"]').nextAll('[data-layout^="ef-program-slot"]').first().find('[data-name="time"]'));
		if (nextMain.$el.length){

			for (var i = efProgramTime.indexOf(nextMain); i < efProgramTime.length; i++){
				setTime(efProgramTime[i], timeDiff);
				setTime(efProgramTimeEnd[i], timeDiff);
			}		
		}
	}

/*	function setTimeEnd(timeEndField, timeDiff){

		var endTime = getDateFromTimeString( timeEndField.val() );
		if (endTime){
			endTime.setTime(endTime.getTime() + timeDiff );
			timeEndField.$inputText().datepicker( 'setTime', endTime );
		}
	}*/

	function setInitialStartTime(field){
		console.log(field);
		// break early if there's no other time field yet
		if (efProgramTime.length < 2){
			return;
		}
		var index = efProgramTime.indexOf(field);
		var lookUpTime = efProgramTimeEnd[index - 1];

		if ( isItemParallel(field) ){

			// get start time of first item from start time of parent parallel block
			if ( isFirstParallelItem(field) ){
				var parent = field.$el.closest('[data-layout="ef-program-slot-parallel"]');
				var parentTimeField = acf.getFields({
					type: 'time_picker',
					name: 'time',
					parent: parent,
					limit: 1
				});
				var time = getDateFromTimeString( parentTimeField[0].val() );
				if (time){
					field.$inputText().datepicker('setTime', time);
					field.set('time', time);
				}
				return;
			}
		// no parallel item
		} else {

			// previous time field is a parallel item
			if ( lookUpTime && isItemParallel(lookUpTime) ){
				// get previous program slot
				lookUpTime = getPrevProgramSlotField(field, 'time_end');
			}
		}

		// if there's no previous element, get start time field of next main slot
		if ( ! lookUpTime ){
			lookUpTime = getNextProgramSlotField(field, 'time');
		}
		var time = getDateFromTimeString( lookUpTime.val() );	

		if ( time ){
			field.$inputText().datepicker( 'setTime', time );
			field.set('time', time);
		}
	}

	function getPrevProgramSlot(currentField){
		return currentField.$el.closest('[data-layout^="ef-program-slot"]').prevAll('[data-layout^="ef-program-slot"]').first();
	}

	function getNextProgramSlot(currentField){
		return currentField.$el.closest('[data-layout^="ef-program-slot"]').nextAll('[data-layout^="ef-program-slot"]').first();
	}

	function getPrevProgramSlotField(currentField, prevName){
		return acf.getField(getPrevProgramSlot(currentField).find('[data-name="'+prevName+'"]'));
	}

	function getNextProgramSlotField(currentField, nextName){
		return acf.getField(getNextProgramSlot(currentField).find('[data-name="'+nextName+'"]'));
	}

	function getFirstParallelIndex(field){
		var firstParallelSibling = field.$el.closest('.values').find('[data-layout^="parallel-sub-item"]').first().find('[data-name="time"]');
		var firstParallelItem = acf.getField(firstParallelSibling);
		return efProgramTime.indexOf(firstParallelItem);
	}

	function isFirstParallelItem(field){
		return efProgramTime.indexOf(field) === getFirstParallelIndex(field);
	}

	function getLastParallelIndex(field){
		var lastParallelSibling = field.$el.closest('.values').find('[data-layout^="parallel-sub-item"]').last().find('[data-name="time"]');
		var lastParallelItem = acf.getField(lastParallelSibling);
		return efProgramTime.indexOf(lastParallelItem);
	}

	function isItemParallel(field){
		// boolean indicating if current item is inside of a parallel block
		return field.$el.closest('.layout').data('layout').indexOf('parallel-sub-item') > -1;
	}

	function preventParallelBlockTimeChange(field){
		var parent = field.$el.closest('.layout');

		if ( parent.data('layout') == 'ef-program-slot-parallel'){
			var parallelItems = acf.getFields({
				name: 'time',
				parent: parent.find('[data-name="parallel-item"]')
			});
			if (parallelItems.length){
				field.showError('Bitte die Endzeit durch Änderung eines Unterelements anpassen.');
			}
		}
	}

	function pad(n){
		return n < 10 ? '0' + n : n;
	}

	function getDateFromTimeString(time){
		if ( typeof time !== 'string' || time.indexOf(':') === -1 ) {
				return false;
		}
		time = time.split(':');
		time = new Date( 0,0,0,time[0], time[1] );

		return time;
	}

})(jQuery);
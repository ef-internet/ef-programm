<?php
function register_cpt_ef_programm() {

	/**
	 * Post Type: Programme.
	 */

	$labels = array(
		"name" => "Programme",
		"singular_name" => "Programm",
	);

	$args = array(
		"label" => "Programme",
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "ef_programm", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-clock",
		"supports" => array( "title" ),
	);

	register_post_type( "ef_programm", $args );
}

add_action( 'init', 'register_cpt_ef_programm' );
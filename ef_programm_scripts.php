<?php

add_action( 'acf/input/admin_enqueue_scripts', function(){

	$ef_auto_time = get_field('ef-program-auto-time');
	wp_enqueue_script( 'ef-programm-acf-admin', plugin_dir_url( __FILE__ ) . 'js/ef-program-acf-admin.js', array( 'jquery', 'jquery-ui-datepicker', 'acf-timepicker' ), filemtime( plugin_dir_path( __FILE__ ) . '/js/ef-program-acf-admin.js') );
	wp_localize_script( 'ef-programm-acf-admin', 'efProgramAutoTime', $ef_auto_time );
});

add_action( 'wp_enqueue_scripts', function(){
	if ( ! BEANS_FRAMEWORK_AVAILABLE ){
		wp_register_script( 'uikit', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/js/uikit.min.js', array( 'jquery' ) );
		wp_register_script( 'uikit-grid', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/js/components/grid.min.js', array( 'uikit' ) );
		wp_register_script( 'uikit-sticky', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/js/components/sticky.min.js', array( 'uikit' ) );
	}
	wp_register_script( 'ef-programm', plugin_dir_url( __FILE__ ) . 'js/ef-program.js', array( 'jquery' ), filemtime( plugin_dir_path( __FILE__ ) . '/js/ef-program.js') );
});
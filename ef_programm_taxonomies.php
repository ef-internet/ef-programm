<?php
function register_ef_programm_kategorie() {

	/**
	 * Taxonomy: Programmpunkt-Arten.
	 */

	$labels = array(
		"name" => "Programmpunkt-Arten",
		"singular_name" => "Programmpunkt-Art",
	);

	$args = array(
		"label" => "Programmpunkt-Arten",
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'ef_programm_typ', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "ef_programm_typ",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"meta_box_cb" => false // hides metabox
		);
	register_taxonomy( "ef_programm_typ", array( "ef_programm" ), $args );
}
add_action( 'init', 'register_ef_programm_kategorie' );

function register_ef_programm_filter() {

	if ( taxonomy_exists( 'speaker_filter' ) ){
		return;
	}

	/**
	 * Taxonomy: Themen-Filter.
	 */

	$labels = array(
		"name" => "Themen-Filter",
		"singular_name" => "Themen-Filter",
	);

	$args = array(
		"label" => "Themen-Filter",
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => false,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'speaker_filter', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "speaker_filter",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		);
	register_taxonomy( "speaker_filter", array( "team-member" ), $args );
}
add_action( 'init', 'register_ef_programm_filter' );
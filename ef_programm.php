<?php
/*
Plugin Name: EF Programm
Description: Erstellt einen neuen Post Type 'Programm' mit entsprechenden Input-Feldern zur Dateneingabe und den shortcode ef_programm zur Ausgabe.
Author: Timo Klemm
Version: 0.6.1
Author URI: https://github.com/team-ok
*/

add_action('after_setup_theme', function(){
	if ( ! defined('BEANS_FRAMEWORK_AVAILABLE') ){
		define( 'BEANS_FRAMEWORK_AVAILABLE', function_exists( 'beans_load_document' ) );
	}
});

add_action('admin_notices', 'ef_programm_plugin_requirements_notice');
function ef_programm_plugin_requirements_notice(){
	if ( ! class_exists('acf_pro') ){ ?>
		<div class="notice notice-error">
			<p>Advanced Custom Fields Pro muss installiert und aktiviert sein, damit EF-Programm funktioniert.</p>
		</div>
<?php }
}

require_once( plugin_dir_path( __FILE__ ) . 'ef_programm_post_type.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_programm_taxonomies.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_programm_fields.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_programm_styles.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_programm_scripts.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_programm_shortcode.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_programm_block.php' );


// set program slot label dynamically
add_filter('acf/fields/flexible_content/layout_title/name=ef-program', 'ef_dynamic_fc_label', 10, 4);
add_filter('acf/fields/flexible_content/layout_title/name=parallel-sub-item', 'ef_dynamic_fc_label', 10, 4);
function ef_dynamic_fc_label($label, $field, $layout, $i){
	
/*	$title = get_sub_field('title');
	$title = $title ? '<span class="ef-program-title-label">' . $title . '</span>' : '';
	$time = get_sub_field('time');
	$time = $time ? '<span class="ef-program-time-label">' . $time . '</span>' : '';
	$type = get_sub_field('type'); // term object
	$type = $type ? '<span class="ef-program-type-label">' . $type->name . '</span>' : '';
		
	if ($time || $title || $type){

		return $time . $type . $title . ' <span class="ef-program-layout-label">(' . $label . ')</span>';
	}*/

	if ( wp_doing_ajax() ){
		die();
	}

	return $label;
}

// modify the args of the speaker ajax request to filter the result by taxonomy
add_filter( 'acf/fields/post_object/query/key=field_5c0fdb9ffbfb9', function($args){

	if ( ! empty($args['s']) ):
		$cat = explode(':cat:', $args['s']);

		if ( count( $cat ) > 1 ){
			if ( trim($cat[0]) !== ''){
				$args['s'] = $cat[0];
			} else {
				unset($args['s']);
			}
			$args['tax_query'] = array();
			$args['tax_query'][] = array(
				'taxonomy'	=> 'team-member-category',
				'field'		=> 'id',
				'terms'		=> $cat[1],
			);
		}

	endif;

	return $args;
});

function ef_get_ics_form($date, $start, $end, $title, $location){
	$form = '';
	if ( $date && $start && $end && $title ){
		$form .= '<form method="post" action="'.plugin_dir_url( __FILE__ ).'ef_programm_ics.php">';
		$form .= '<input type="hidden" name="start" value="'.$date.' '.$start.':00">';
		$form .= '<input type="hidden" name="end" value="'.$date.' '.$end.':00">';
		$form .= '<input type="hidden" name="title" value="'.$title.'">';
		$form .= $location ? '<input type="hidden" name="location" value="'.$location.'">' : '';
		$form .= '<input type="hidden" name="event-title" value="'.get_bloginfo('name').'">';
		$form .= '<button class="uk-button ef-add-to-cal" type="submit"><i class="uk-icon-calendar-plus-o"></i></button>';
		$form .= '</form>';
	}
	return $form;
}
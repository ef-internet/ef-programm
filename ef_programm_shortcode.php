<?php
add_shortcode( 'ef_programm', 'ef_get_program' );
function ef_get_program( $atts, $content = "", $shortcode_tag ){

	if ( ! class_exists( 'acf_pro' ) ){
		return '';
	}

	$page_url = get_permalink();

	$atts = shortcode_atts( array(

		'id' => array(),
		'ids' => array(),
		'template' => 'list, grid', // alternative values: 'grid' or 'list' (disables template switch view)
		'switch_labels' => '',  // comma separated list of labels for the template switcher. If empty, template names will be used as default labels
		'switch_icons' => 'list, th',  // comma separated list of uk-icon names (see https://getuikit.com/v2/docs/icon.html#icon-mapping for available names)
		'filter_active' => '', // default active filter, possible values: '' (show all), number (post id of program)
		'sticky_nav' => 'true', // make filter navigation stick to the top of the page when scrolling
		'showall_label' => 'Alles', // label of the 'Show all'-button (grid view)
		'separator' => ', ',  // to separate between title and position
		'autocat' => 'no', // get thematic categories (filter) from selected speakers automatically
		'pdf' => 'no', // show printerfriendly button (printerfriendly plugin must be installed and activated)
		'pdf_button_title' => 'Print & PDF',
		'quicksearch' => 'no'

	), $atts, $shortcode_tag );

	$atts['ids'] = !empty($atts['ids']) ? $atts['ids'] : $atts['id'];
	$atts['ids'] = ! is_array($atts['ids']) ? explode(',', $atts['ids']) : $atts['ids'];

	$args = array(
		'post_type' => 'ef_programm',
		'numberposts' => -1,
		'include' => $atts['ids'],
		'orderby' => 'post__in',
		'order' => 'ASC'
	);

	$program = get_posts( $args );

	if ( ! $program ){
		return '';
	}

	$args_hash = substr( md5( serialize( $atts ) ), 0, 8 ); // create hash value of $atts array to be used as dynamic filter id
	$templates = explode( ',', $atts['template'] );
	$templates = array_map( 'trim', $templates );
	$output = '';
	$switch_templates = count( $templates ) > 1;
	$print_class = $atts['pdf'] !== 'no' ? ' print-only' : '';
	$speaker_objects = array();
	$items = array();
	$topic_filter = array();
	$tab_filter = array();
	$js_options = array();
	$js_options['pdf'] = $atts['pdf'] != 'no' ? true : false;
	$js_options['quicksearch'] = $atts['quicksearch'] != 'no' ? true : false;
	// set initial grid width and view classes depending on query parameters (higher priority) or order of shortcode template attribute
	$grid_width_classes = ' uk-grid-width-1-1';
	$view_class = ' ef-programm-list-view';

	if ( isset($_GET['view']) ){
		if ( $_GET['view'] == 'grid'){
			$grid_width_classes = ' uk-grid-width-medium-1-2 uk-grid-width-large-1-3';
			$view_class = ' ef-programm-grid-view';
		}
	} elseif ( $templates[0] == 'grid' ){
		$grid_width_classes = ' uk-grid-width-medium-1-2 uk-grid-width-large-1-3';
		$view_class = ' ef-programm-grid-view';
	}
	$multi_class = count( $atts['ids'] ) > 1 ? ' ef-programm-multiple' : '';

	$output .= '<div id="ef-programm-' . $args_hash . '" class="ef-programm-main-wrapper' . $view_class . $multi_class . '">' . "\n";

	if ( $switch_templates || $atts['pdf'] !== 'no' || $atts['quicksearch'] != 'no' ):

		$output .= '<div class="uk-button-group">' . "\n";

			if ( $switch_templates ){

				$labels = explode( ',', $atts['switch_labels'] );
				$icons = explode( ',', $atts['switch_icons'] );
				$icons = array_map( 'trim', $icons );

				foreach ( $templates as $key => $template ) {
					$label = ! empty( $labels[$key] ) ? $labels[$key] : '';
					$icon = ! empty( $icons[$key] ) ? '<i class="uk-icon-' . esc_attr( $icons[$key] ) . ' uk-margin-small-right"></i>' : '';
					// preselect view template dynamically
					$state_class = $key === 0 ? ' uk-active' : '';
					if ( isset( $_GET['view'] ) ){
						if ( $_GET['view'] === $template ){
							$state_class = ' uk-active';
						} else {
							$state_class = '';
						}
					}
					$output .= '<button class="uk-button ' . esc_attr($template) . $state_class . '" type="button">' . $icon . esc_html( $label ) . '</button>' . "\n";
				}
			}

			if ( $atts['pdf'] !== 'no' && function_exists('pf_show_link') ){

				$js_options['pdf'] = true;

				// change printfriendly button title
				add_filter( 'printfriendly_button', function($html) use ($atts){
					return str_replace('title="Printer Friendly, PDF & Email"', 'title="'.esc_attr($atts['pdf_button_title']).'"', $html);
				});

				$output .= pf_show_link();
			}

			if ( $atts['quicksearch'] != 'no' ){
				$output .= '<form action="" class="uk-form uk-text-right"><input class="uk-search-field" type="search" id="ef-programm-search-'.$args_hash.'" placeholder="Schnellsuche ..."></form>';
			}

		$output .= '</div>' . "\n";

	endif;
	
	ob_start();

	// preselect filter dynamically
	if ( isset( $_GET['filter'] ) && strpos( $_GET['filter'], 'id-') === 0 ){
		$filter_active = $_GET['filter'];
	} elseif ( $atts['filter_active'] ) {
		$filter_active = 'id-' . $atts['filter_active'];
	} else {
		$filter_active = '';
	}
	?>
	<div class="ef-programm">

		<?php 	
			$sticky_nav = $atts['sticky_nav'] === 'true' ? ' data-uk-sticky="{boundary:true}"' : ''; ?>

			<ul id="ef-topics-filter-<?php echo $args_hash; ?>" class="uk-subnav uk-subnav-pill ef-dynamic-grid-filter"<?php echo $sticky_nav; ?>>
				%%FILTER_NAV_LIST%%
			</ul>
			<h2 class="ef-programm-pf-filter-title print-only"></h2>
			<div class="uk-grid<?php echo $grid_width_classes; ?>" data-uk-grid="{gutter: 10, controls: '#ef-topics-filter-<?php echo $args_hash; ?>', filter: <?php echo '\'' . esc_attr($filter_active) . '\''; ?>}">

		<?php global $post;

			foreach ($program as $post):

				setup_postdata( $post );

					$date = get_field('ef-program-date', $post->ID);
					$location = get_field('ef-program-location', $post->ID);

					$items['id-' . $post->ID] = $post->post_title;

					if ( have_rows( 'ef-program', $post->ID ) ):

						while ( have_rows( 'ef-program', $post->ID ) ): the_row();

							$data = array();
							$data['slot_type'] = get_row_layout();
							$classes = array();
							$classes[] = $data['slot_type'];
							$index = get_row_index();

							switch ( $data['slot_type'] ):

								/*----------  normal program slot  ----------*/
								
								case 'ef-program-slot':

									include 'inc/programm-slot-data.php';

									// wrap content in grid div
									include 'inc/programm-slot-open.php';

									$speakers = get_sub_field( 'speaker' ); // post object/array of post objects
									include 'inc/programm-slot-speaker-template.php';

									include 'inc/programm-slot-close.php';

									break;
								/*----------  end of normal program slot  ----------*/

								/*----------  cluster porgram slot  ----------*/
									
								case 'ef-program-slot-cluster':

									include 'inc/programm-slot-data.php';
									// wrap content in grid div
									include 'inc/programm-slot-open.php';

										$data['cluster_items'] = get_sub_field( 'cluster-items' ); // array

										if ( $data['cluster_items'] ):

											echo '<div class="ef-programm-cluster uk-panel-box">';

											foreach ( $data['cluster_items'] as $item ):

												echo '<div class="ef-programm-cluster-item">';

													if ( $item['title'] ){ ?>
														<h5 class="ef-programm-cluster-item-title"><?php echo $item['title']; ?></h5>
													<?php }
													if ( $item['text_before_speakers_select'] && $item['text_before_speakers'] ){ ?>
														<div class="ef-programm-cluster-item-text"><?php echo $item['text_before_speakers']; ?></div>
													<?php }

													$speakers = $item['speaker'];
													include 'inc/programm-slot-speaker-template.php';

												echo '</div>';

											endforeach;

											echo '</div>';

										endif;

									include 'inc/programm-slot-close.php';

									break;
								/*----------  end of cluster program slot  ----------*/

								/*----------  parallel program slot  ----------*/
								case 'ef-program-slot-parallel':

									include 'inc/programm-slot-parallel.php';

									break;

								/*----------  end of parallel program slot  ----------*/

								/*----------  section program slot  ----------*/
								case 'ef-program-section':

									$data['title'] = get_sub_field( 'title' );
									$this_filter = array( 'id-' . $post->ID );
									$data['category'] = get_sub_field( 'category' ); // term id
									$data['category'] = $data['category'] ? $data['category'] : array();

									foreach ( $data['category'] as $filter_cat ) {
										$topic_filter['id-' . $filter_cat->term_id] = $filter_cat->name;
										$this_filter[] = 'id-' . $filter_cat->term_id;
										$classes[] = $filter_cat->slug;
									}
									?>
									<div class="<?php echo esc_attr( implode(' ', $classes) ); ?>" data-uk-filter="<?php echo implode( ',', $this_filter ); ?>">
										<div class="uk-panel-box">
											<h3><?php echo $data['title']; ?></h3>
											<?php 
											if ( $data['category'] ){
												$tags = array();
												echo '<div class="ef-programm-slot-tags">';
													foreach ($data['category'] as $cat) {
														$tags[] = $cat->name;
													}
													echo '<span>'. implode(', ', $tags) .'</span>';
												echo '</div>';
											}
											?>
										</div>
									</div>
									<?php
									break;
								/*----------  end of section program slot  ----------*/

								/*----------  text program slot  ----------*/
								case 'ef-program-text':

									$data['title'] = get_sub_field( 'title' );
									$data['type'] = get_sub_field( 'type' ); // term id
									$data['category'] = get_sub_field( 'category' ); // term id
									$data['text'] = get_sub_field( 'text' );
									$this_filter = array( 'id-' . $post->ID );
									$data['category'] = $data['category'] ? $data['category'] : array();

									foreach ( $data['category'] as $filter_cat ) {

										$topic_filter['id-' . $filter_cat->term_id] = $filter_cat->name;
										$this_filter[] = 'id-' . $filter_cat->term_id;
										$classes[] = $filter_cat->slug;
									}

									if ( $data['type'] ){
										$classes[] = $data['type']->slug;
									}
									?>

									<div class="<?php echo esc_attr( implode(' ', $classes) ); ?>" data-uk-filter="<?php echo implode( ',', $this_filter ); ?>">

										<div class="uk-panel-box">

										<?php

											if ( $data['type'] ){
												echo '<span class="ef-programm-type">' . $data['type']->name  . '</span>';
											}
											if ( $data['title'] ){
												echo '<h4>' . $data['title'] . '</h4>';
											}

											if ( $data['text'] ){
												echo '<div class="ef-programm-text">';
													echo apply_filters( 'the_content', $data['text'] );
												echo '</div>';
											}

											if ( $data['category'] ){
												$tags = array();
												echo '<div class="ef-programm-slot-tags">';
													foreach ($data['category'] as $cat) {
														$tags[] = $cat->name;
													}
													echo '<span>'. implode(', ', $tags) .'</span>';
												echo '</div>';
											}
										?>

										</div>
									</div>

								<?php break;
								/*----------  end of text program slot  ----------*/

							endswitch;

						endwhile;

					endif;

				wp_reset_postdata();

			endforeach; 

		echo '</div>'; // close grid-wrapper

	echo '</div>'; // close ef-programm

	$html = ob_get_clean();

	$html = apply_filters( 'ef_programm_html_output_before_navigation', $html, $atts );


	// build filter navigation

	// remove doubles
	$topic_filter = array_unique( $topic_filter );

	// sort values ascending
	asort($topic_filter);

	// merge program names with topic names
	$filter = apply_filters( 'ef_programm_filter_nav_items', array_merge( $items, $topic_filter ) );

	// merge in tab filters
	$filter = array_merge($filter, $tab_filter);

	$filter_nav_list = '<li data-uk-filter=""><a href="#">'. esc_html( $atts['showall_label'] ) .'</a></li>' . "\n";

	foreach ($filter as $filter_id => $filter_name) {
		$class = isset($items[$filter_id]) ? 'ef-programm-main-filter' : 'ef-programm-cat-filter';
		$filter_atts = strpos($filter_id , 'ef-tab') === false ? ' data-uk-filter="' . esc_attr($filter_id) . '"' : ' data-ef-tab-filter="'.$filter_id.'"';
		$filter_nav_list .= '<li class="' . $class . '"' . $filter_atts . '><a href="#">' . esc_html($filter_name) . '</a></li>' . "\n";
	}

	$filter_nav_list = apply_filters( 'ef_programm_filter_navigation', $filter_nav_list, $atts );

	$html = str_replace( '%%FILTER_NAV_LIST%%', $filter_nav_list, $html );

	$output .= $html;

	$output = apply_filters( 'ef_programm_html_output_before_modal', $output, $atts );

	// speaker modals
	if ( !empty( $speaker_objects ) ):

		ob_start();

		echo '<div id="ef-speaker-modal-container-'.$args_hash.'">';

		foreach ($speaker_objects as $speaker) {

			$speaker_image = get_the_post_thumbnail( $speaker->ID, 'medium' );
			$speaker_function = get_field( 'ef-speaker-function', $speaker->ID );
			$speaker_partner = get_field( 'ef-speaker-partner', $speaker->ID );
			$speaker_company = get_field( 'ef-speaker-company', $speaker->ID );
			$separator = $speaker_company ? $atts['separator'] : '';

			echo '<div id="ef-speaker-modal-'.$speaker->ID.'-'.$args_hash.'" class="uk-modal ef-programm-modal">';
				echo '<div class="uk-modal-dialog">';
					echo '<a class="uk-modal-close uk-close"></a>';
					echo '<div class="uk-grid" data-uk-grid-margin>';
						echo '<div class="uk-width-medium-1-3 uk-flex-item-auto">';
							if ( $speaker_image ){
								echo $speaker_image;
							}
							echo '<h3>'.$speaker->post_title.'</h3>';
							if ( $speaker_function ){
								echo '<span class="ef-speaker-position">' . $speaker_function .  esc_html( $separator ) . '</span>';			
							}

							if ( ! empty($speaker_partner) && $speaker_partner instanceof WP_Post ):

								$name = $speaker_company ? $speaker_company : $speaker_partner->post_title;
								
								echo '<span class="ef-speaker-company">' . esc_html($name) . '</span>';

							elseif ( $speaker_company ):

								echo '<span class="ef-speaker-company">' . esc_html($speaker_company) . '</span>';

							endif;

						echo '</div>';
						if ( !empty( $speaker->post_content ) ){
							echo '<div class="uk-width-medium-2-3">';
								echo '<div class="ef-speaker-info">';
									echo apply_filters( 'the_content', $speaker->post_content );
								echo '</div>';
							echo '</div>';
						}
					echo '</div>';
				echo '</div>';
			echo '</div>';
		}
		echo '</div>';

		$output .= ob_get_clean();
	endif;

	$output .= '</div>' . "\n"; // close ef-programm-main-wrapper

	$output = apply_filters( 'ef_programm_html_output', $output, $atts );

	// enqueue frontend assets
	if ( ! BEANS_FRAMEWORK_AVAILABLE ){
		wp_enqueue_script( 'uikit' );
		wp_enqueue_script( 'uikit-grid' );
		wp_enqueue_script( 'uikit-sticky' );

		wp_enqueue_style( 'uikit' );
		wp_enqueue_style( 'uikit-sticky' );
		wp_enqueue_style( 'ef-programm' );
	}
	wp_enqueue_script( 'ef-programm' );
	wp_add_inline_script( 'ef-programm', 'var efProgramOptions = '.json_encode($js_options).';', 'before' );

	return $output;
}
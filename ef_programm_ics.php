<?php
require_once( dirname( __FILE__ ) . '/vendor/autoload.php' );

// set default timezone (PHP 5.4)
//date_default_timezone_set('Europe/Berlin');

// 1. Create new calendar
$vCalendar = new \Eluceo\iCal\Component\Calendar('www.euroforum.de');

// 2. Create an event
$vEvent = new \Eluceo\iCal\Component\Event();
$vEvent->setDtStart(new \DateTime($_POST['start']));
$vEvent->setDtEnd(new \DateTime($_POST['end']));
$vEvent->setSummary($_POST['title'] . ' | ' . $_POST['event-title'] );
$vEvent->setDescription('Test');
$vEvent->setUrl($_SERVER['HTTP_REFERER']);

if ( $_POST['location'] ){
	$vEvent->setLocation($_POST['location']);
}

// Adding Timezone (optional)
//$vEvent->setUseTimezone(true);

// 3. Add event to calendar
$vCalendar->addComponent($vEvent);

// 4. Set headers
header('Content-Type: text/calendar; charset=utf-8');
header('Content-Disposition: attachment; filename="cal.ics"');

// 5. Output
echo $vCalendar->render();
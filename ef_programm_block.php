<?php
add_action('acf/init', 'register_ef_programm_block');
function register_ef_programm_block(){

	if ( ! function_exists( 'acf_register_block_type') ){
		return;
	}

	acf_register_block_type( array(
		'name' => 'ef-programm-block',
		'title' => 'Programm',
		'render_callback' => 'ef_programm_block_render',
		'category' => 'widgets',
		'icon' => 'clock',
		'mode' => 'auto',
		'align' => 'wide',
		'supports' => array(
			'align' => true
		)
	));
}
function ef_programm_block_render($block, $content = '', $is_preview = false, $post_id = 0){

	if ( ! function_exists( 'ef_get_program') ){
		return;
	}

	$data = array();
	$data['ids'] = get_field('ids') ?: array();
	$data['filter_active'] = get_field('filter_active') ?: '';
	$data['template'] = get_field('template');
	$data['pdf'] = get_field('pdf') ? 'yes' : 'no';
	$data['pdf_button_title'] = get_field('pdf_button_title') ?: 'Print & PDF';
	$data['sticky_nav'] = get_field('sticky_nav') ? 'true' : 'false';
	$data['quicksearch'] = get_field('quicksearch') ? 'yes' : 'no';
	$data['autocat'] = get_field('autocat') ? 'yes' : 'no';
	$data['switch_labels'] = implode( ',', array_filter( get_field('switch_labels') ) );
	$data['switch_icons'] = implode( ',', array_filter( get_field('switch_icons') ) );
	$data['showall_label'] = get_field('showall_label');
	$data['separator'] = get_field('separator');

	echo ef_get_program( $data, '', 'ef_programm' );

}
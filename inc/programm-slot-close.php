<?php
		if ( $data['text'] && $data['text_after_speakers_select'] ){
			echo '<div class="ef-programm-text ef-programm-text-after-speakers">';
				echo apply_filters( 'the_content', $data['text'] );
			echo '</div>';
		}

		if ( $data['category'] ){
			$tags = array();
			echo '<div class="ef-programm-slot-tags">';
				foreach ($data['category'] as $cat) {
					$tags[] = $cat->name;
				}
				echo '<span>'. implode(', ', $tags) .'</span>';
			echo '</div>';
		}

	// close panel-box ?>
	</div>
<?php // close grid wrapper div ?>
</div>
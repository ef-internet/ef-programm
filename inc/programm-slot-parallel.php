<?php
include 'programm-slot-data.php';
// wrap content in grid div
include 'programm-slot-open.php';

	$data['parallel_items'] = get_sub_field('parallel-item');

	if ( $data['parallel_items'] ):

		$js_options['parallel_items'] = true; ?>

		<div class="ef-programm-parallel uk-panel-box">
			<ul class="uk-tab" data-uk-tab="{connect:'#ef-parallel-block-<?php echo $args_hash.'-'.$index; ?>'}">
				<?php foreach ( $data['parallel_items'] as $block ) { ?>
					<li>
						<a href=""><?php echo $block['tab-title'] ?: $block['title']; ?></a>
					</li>
				<?php } ?>
			</ul>
			<ul id="ef-parallel-block-<?php echo $args_hash.'-'.$index; ?>" class="ef-parallel-blocks uk-switcher uk-margin-top">
				<?php foreach ( $data['parallel_items'] as $block_index => $block ): 
					$tab_filter_key = '';
					if ( $block['filter-button']){
						$tab_filter_key = 'ef-tab-'.$args_hash.'-'.$index.'-'.$block_index; 
						$label = $block['tab-title'] ?: $block['title'];
						$tab_filter[$tab_filter_key] = $label;
					}
					?>
					<li class="ef-programm-parallel-item" data-ef-tab-filter="<?php echo $tab_filter_key; ?>">
						<h5><?php echo $block['title']; ?></h5>
						<?php if ( $block['text_before_speakers_select'] && $block['text_before_speakers'] ): ?>

							<div class="ef-parallel-block-text">
								<?php echo $block['text_before_speakers']; ?>
							</div>

						<?php endif;
						if ( !empty( $block['parallel-sub-item'] ) ):
							foreach ( $block['parallel-sub-item'] as $item ): ?>
								<div class="uk-panel-box ef-program-<?php echo esc_attr($item['acf_fc_layout']); ?>">
									<?php

									switch ( $item['acf_fc_layout'] ):

										case 'parallel-sub-item-standard':
										case 'parallel-sub-item-parallel':

											if ( $item['time'] ){
												echo '<span class="ef-programm-time">';
													echo '<span class="ef-programm-time-prefix">'. esc_html( $item['time_prefix'] ) .'</span>';
													echo esc_html( $item['time'] );
													echo '<span class="ef-programm-time-suffix">'. esc_html( $item['time_suffix'] ) .'</span>';
												echo '</span>';
												echo ef_get_ics_form($date, $item['time'], $item['time_end'], $item['title'], $item['location'] ?: $location);
											}
											if ( $item['type'] ){
												echo '<span class="ef-programm-type">' . $item['type']->name  . '</span>';
											}
											if ( $item['title'] ){ ?>
												<h6><?php echo $item['title']; ?></h6>
											<?php }

											if ( $item['text'] ){ ?>
												<div class="ef-programm-text">
													<?php echo $item['text']; ?>
												</div>
											<?php }
											// standard type only
											if ( isset($item['speaker']) && $item['speaker'] ){
												$speakers = $item['speaker'];
												include 'programm-slot-speaker-template.php';
											}
											// parallel type only
											if ( isset($item['sub-sub-items']) && $item['sub-sub-items'] ):

												echo '<div class="ef-programm-parallel-sub-sub-items uk-panel-box">';

												foreach ( $item['sub-sub-items'] as $sub_item ):

													echo '<div class="ef-programm-parallel-sub-sub-item">';

														if ( $sub_item['title'] ){ ?>
															<h6 class="ef-programm-parallel-sub-sub-item-title"><?php echo $sub_item['title']; ?></h6>
														<?php }
														if ( $sub_item['text_before_speakers_select'] && $sub_item['text_before_speakers'] ){ ?>
															<div class="ef-programm-parallel-sub-sub-item-text"><?php echo $sub_item['text_before_speakers']; ?></div>
														<?php }

														$speakers = $sub_item['speaker'];
														include 'programm-slot-speaker-template.php';

													echo '</div>';

												endforeach;

												echo '</div>';

											endif;

											break;

										case 'section':

											echo '<h3>' . $item['title'] . '</h3>';

											break;

									endswitch;
									?>
								</div>
							<?php endforeach;
						endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div> 

	<?php endif;

include 'programm-slot-close.php';
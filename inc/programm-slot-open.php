<div class="<?php echo esc_attr( implode(' ', $classes) ); ?>" data-uk-filter="<?php echo implode( ',', $this_filter ); ?>">

	<div class="uk-panel-box">

		<?php
		// display topic cat label only if there are multiple program posts queried
		if ( count($atts['ids']) > 1 ){
			echo '<span class="ef-topic-cat">'. $post->post_title .'</span>';
		}
		if ( $data['time'] ){
			echo '<div class="ef-programm-time-wrapper">';
				echo '<span class="ef-programm-time">';
					echo '<span class="ef-programm-time-prefix">'. esc_html( $data['time_prefix'] ) .'</span>';
					echo esc_html( $data['time'] );
					echo '<span class="ef-programm-time-suffix">'. esc_html( $data['time_suffix'] ) .'</span>';
				echo '</span>';
				echo ef_get_ics_form($date, $data['time'], $data['time_end'], $data['title'], $data['location'] ?: $location);
			echo '</div>';
		}
		if ( $data['type'] ){
			echo '<span class="ef-programm-type">' . $data['type']->name  . '</span>';
		}
		if ( $data['title'] ){ ?>
			<h4><?php echo $data['title']; ?></h4>
		<?php }

		if ( $data['text_before_speakers'] && $data['text_before_speakers_select'] ){
			echo '<div class="ef-programm-text ef-programm-text-before-speakers">';
				echo apply_filters( 'the_content', $data['text_before_speakers'] );
			echo '</div>';
		}
<?php
$position = get_field( 'ef-speaker-function', $speaker->ID );
$company = get_field( 'ef-speaker-company', $speaker->ID );
if ( ! $company ){
	$partner = get_field( 'ef-speaker-partner', $speaker->ID );
	$company = $partner instanceof WP_Post ? $partner->post_title : '';
}
$separator = $company ? $atts['separator'] : '';
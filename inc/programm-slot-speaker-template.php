<?php

if ( !empty( $speakers ) ): ?>

	<div class="uk-grid uk-grid-width-medium-1-2" data-uk-grid-margin>

		<?php foreach ( $speakers as $speaker ) {

			$speaker_objects[$speaker->ID] = $speaker;

			if ( $atts['autocat'] === 'yes' ){
				include 'programm-slot-speaker-cats.php';
			}

			include 'programm-slot-speaker-data.php';
			include 'programm-slot-speaker-html.php';
		} ?>
	</div>
<?php endif; ?>
<?php
$data['time'] = get_sub_field( 'time' );
$data['time_end'] = get_sub_field( 'time_end' );
$data['time_prefix'] = get_sub_field( 'time_prefix' );
$data['time_suffix'] = get_sub_field( 'time_suffix' );
$data['title'] = get_sub_field( 'title' );
$data['type'] = get_sub_field( 'type' ); // term object
$data['category'] = get_sub_field( 'category' ); // term object / array of term objects
$data['location'] = get_sub_field( 'location' );
$data['text_before_speakers'] = get_sub_field('text_before_speakers');
$data['text_before_speakers_select'] = get_sub_field('text_before_speakers_select'); // bool
$data['text'] = get_sub_field( 'text' );
$data['text_after_speakers_select'] = get_sub_field('text_after_speakers_select'); // bool

$this_filter = array( 'id-' . $post->ID );
$data['category'] = $data['category'] ? $data['category'] : array();

foreach ( $data['category'] as $filter_cat ) {

	$topic_filter['id-' . $filter_cat->term_id] = $filter_cat->name;
	$this_filter[] = 'id-' . $filter_cat->term_id;
	$classes[] = $filter_cat->slug;
}

if ( $data['type'] ){
	$classes[] = $data['type']->slug;
}
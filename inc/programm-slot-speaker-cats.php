<?php
$speaker_cats = wp_get_post_terms( $speaker->ID, 'speaker_filter' );

if ( ! is_wp_error( $speaker_cats ) && ! empty( $speaker_cats ) ):

	foreach ($speaker_cats as $speaker_cat) {
		$key = 'id-' . $speaker_cat->term_id;

		if ( ! array_key_exists( $key, $topic_filter ) ){
			$topic_filter[$key] = $speaker_cat->name;
		}
		if ( ! array_key_exists( $key, $this_filter ) ){
			$this_filter[] = $key;
		}
	}

endif;
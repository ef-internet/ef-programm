<div class="ef-programm-person">
	<a href="#ef-speaker-modal-<?php echo esc_attr( $speaker->ID ) . '-' . $args_hash; ?>" data-uk-modal={center:true}>
		<figure>
			<?php echo get_the_post_thumbnail( $speaker->ID, 'medium' ); ?>
			<figcaption>
				<span class="ef-programm-person-title"><?php echo esc_html( $speaker->post_title ); ?></span>
				<?php if ( $position ){ ?>
					<span class="ef-programm-person-position"><?php echo esc_html( $position ) . esc_html( $separator ); ?></span>
				<?php }
				if ( $company){ ?>
					<span class="ef-programm-person-company"><?php echo esc_html( $company ); ?></span>
				<?php } ?>
			</figcaption>
		</figure>
	</a>
</div>